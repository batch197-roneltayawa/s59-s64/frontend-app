// React Components or Dependencies
import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';

// Components
import AppNavBar from './components/AppNavBar';

// Pages
import AddProduct from './pages/AddProduct';
import AdminDashboard from './pages/AdminView';
import Products from './pages/Products';
import ProductView from './pages/ProductView';
import EditProduct from './pages/EditProduct'

// Home page
import Home from './pages/Home';
// import Error from './pages/Error'
//Registry page
import Register from './pages.user/Register';
import Login from './pages.user/Login';
import Logout from './pages.user/Logout';


import './App.css';
import { UserProvider } from './UserContext';

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin:null
  });

  const unsetUser = () => {
    localStorage.clear ()
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {Authorization: `Bearer ${localStorage.getItem('token')}`
    }
  })
  .then( res => res.json())
  .then(data => {

    if(typeof data._id !== "undefined"){
      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      })
    }else{
      setUser({
        id:null,
        isAdmin: null
      })
    }
  })
  }, [])

  return (
    
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavBar />
        <Container>
          <Routes>
            <Route path="/" element={<Home/>} />

            {/* User Function */}
            <Route path="/login" element={<Login/>} />
            <Route path="/logout" element={<Logout/>} />
            <Route path="/register" element={<Register/>} />

            {/*Admin View*/}
            <Route path="/admin" element={<AdminDashboard/>} />
            <Route path="/addProduct" element={<AddProduct/>} /> 
            <Route path="/editProduct/:productId" element={<EditProduct/>} />

            {/*User View*/}
            <Route path="/products" element={<Products/>} />
            <Route path='/products/:productId' element={<ProductView/>} />
            
            {/* <Route path="*" element={<Error/>} /> */}
          </Routes>
        </Container>
      </Router>
    </UserProvider>

  );
}

export default App;
