import { useContext,useState, useEffect } from "react";
import { Table, Button } from "react-bootstrap";
import { Navigate,Link } from "react-router-dom";

import UserContext from "../UserContext";

import Swal from "sweetalert2";


export default function Admin(){

	// to validate the user role.
	const {user} = useContext(UserContext);
	// console.log('userContext');
	// console.log(user);
	const [ allProducts, setAllProducts] = useState([])	

	// eslint-disable-next-line react-hooks/exhaustive-deps
	const getAllProducts = () => {
		fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
			.then((res) => res.json())
			.then((data) => {
				setAllProducts(data.map(product => {
					return(
						<tr key={product._id}>
							<td>{product.image}</td>
							<td>{product.name}</td>
							<td>{product.description}</td>
							<td>{product.price}</td>
							<td className="">{product.isActive ? "Active" : "Inactive"}</td>
							<td className="button">
								{
									// We use conditional rendering to set which button should be visible based on the product status (active/inactive)
									(product.isActive)
									?	
										 // A button to change the product status to "Inactive"
										<Button variant="primary" size="sm" className="button" onClick ={() => archive(product._id, product.name)}>Archive</Button>
									:
										<>
											{/* A button to change the product status to "Active"*/}
											<Button variant="primary" size="sm" className="button" onClick ={() => unarchive(product._id, product.name)}>Unarchive</Button>
											{/* A button to edit a specific product*/}
											<Button as={ Link } to={`/editProduct/${product._id}`} variant="primary" size="sm" className="m-2 button" >Edit</Button>
										</>
								}
							</td>
						</tr>
					)
				}))	
			})
		}

	// Making the product inactive
	const archive = async (productId, productName) => {

		const response = await fetch(`${process.env.REACT_APP_API_URL}/products/archive/${productId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`,
			},
			body: JSON.stringify({
				isActive: false,
			}),
		})

		const jsonResponse = await response.json();
		
			
			
				console.log(jsonResponse)
			

				if (jsonResponse) {
					Swal.fire({
						title: 'Archive Successful!',
						icon: 'success',
						text: `${productName} is now inactive.`,
					})
					
				} else {
					Swal.fire({
						title: 'Archive Unsuccessful!',
						icon: 'error',
						text: `Something went wrong. Please try again later!`,
					})
				}
			
		}

	// Making the product active
	const unarchive = async (productId, productName) => {
		console.log('you are here');
		console.log(productId);
		
		const response = await 	fetch(`${process.env.REACT_APP_API_URL}/products/active/${productId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`,
			},
			body: JSON.stringify({
				isActive: true,
			}),
		})

		const jsonResponse = await response.json();
		console.log(jsonResponse);
	
				if (jsonResponse) {
					Swal.fire({
						title: 'Success',
						icon: 'success',
						text: `${productName} is now active.`,
					})
				} else {
					Swal.fire({
						title: 'Unarchive Unsuccessful!',
						icon: 'error',
						text: `Something went wrong. Please try again later!`,
					})
				}

		}
	useEffect(() => {
		// invoke fetchData() to get all products.
		getAllProducts()
	}, [getAllProducts])

	return (
		(user.isAdmin)
		?
		<>
			<div className="mt-5 mb-3 text-center">
				<h1 className="header">Admin Dashboard</h1>
				{/*A button to add a new product*/}
				<Button
					as={Link}
					to="/addProduct"
					variant="primary"
					size="lg"
					className="mx-2"
				>
					Add Product
				</Button>
				<Button 
					as={Link}
					to="/orders"
					variant="primary" 
					size="lg" 
					className="mx-2">

					Show Orders
				</Button>
			</div>
			<Table striped bordered hover>
				<thead>
					<tr>
						<th>Image</th>
						<th>Product Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					{allProducts} 
				</tbody>
			</Table>
		</>
		:
		<Navigate to="/admin" />
	)
}
