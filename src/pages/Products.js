import { Fragment, useEffect, useState, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import { Row } from 'react-bootstrap'
import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';


export default function Products(){

    const [products, setProducts] = useState([])

    const user = useContext(UserContext)

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/`)
        .then( res => res.json())
        .then (data => {

            setProducts(data)
            })
    }, [])

    return(
        (user.isAdmin)
        ?
            <Navigate to="/admin"/>
        :
            <Fragment>
                <h1 className="text-center my-3">Products</h1>
                <Row className="d-flex m-3">
                    {products.map((product) => (
				    <ProductCard key={product._id} productProp={product} />
			        ))}
                </Row>
                
            </Fragment>
    )
}
