import { useContext } from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar() {

    const { user } = useContext(UserContext)
	// console.log(user)
    
    return(
        <Navbar expand="lg" className="Navbar">
		    <Container fluid>
            <Navbar.Brand as={Link} to="/" className="title">Iyaman</Navbar.Brand>
		      <Navbar.Toggle aria-controls="navbarScroll" />
		      <Navbar.Collapse id="navbarScroll">
		        <Nav className="ms-auto navHead">
					{
						(user.isAdmin)
						?
						<Nav.Link as={Link} to="/admin">Admin View</Nav.Link>
						:
						<>
							<Nav.Link as={Link} to="/products">Products</Nav.Link>
							<Nav.Link as={Link} to="/orders">Orders</Nav.Link>
						</>
					}
		          	{ (user.id !== null) ?
						<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
						:
						<>
						<Nav.Link as={Link} to="/login">Sign in</Nav.Link>
						<Nav.Link as={Link} to="/register">Join now</Nav.Link>
						</>
				  	} 
		        </Nav>
		      </Navbar.Collapse>
		    </Container>
		</Navbar>
    )
}
