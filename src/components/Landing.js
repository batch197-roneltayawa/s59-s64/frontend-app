import { Carousel } from 'react-bootstrap';

export default function Landing() {

    return (
        <Carousel interval={null} className="landing">
            <Carousel.Item>
                <img
                className="landing-img"
                src="https://www.tastingtable.com/img/gallery/11-types-of-whiskey-explained/intro-1657456225.webp"
                alt="First slide"
                />
                <Carousel.Caption>
                <h3>Whiskey Worth Knowing</h3>
                <p>It's time you take a break. Fall in love in just one sip</p>
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
                <img
                className="landing-img"
                src="https://www.tastingtable.com/img/gallery/11-types-of-whiskey-explained/japanese-whisky-1657456225.webp"
                alt="Second slide"
                />

                <Carousel.Caption>
                <h3>Nikka Whisky From the Barrel</h3>
                <p>Explore Japanese whisky often aged in Japanese oak barrels</p>
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
                <img
                className="landing-img"
                src="https://static01.nyt.com/images/2022/01/13/dining/13pour1/13pour1-superJumbo.jpg?quality=75&auto=webp"
                alt="Third slide"
                />

                <Carousel.Caption>
                <h3>Wine is life</h3>
                <p>
                    Wine brightens the life and thinking of anyone
                </p>
                </Carousel.Caption>
            </Carousel.Item>
        </Carousel>
    )
}